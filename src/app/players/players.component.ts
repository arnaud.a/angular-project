import { Component, OnInit } from '@angular/core';
import {JexiaService} from '../jexia.service';

@Component({
  selector: 'app-players',
  templateUrl: './players.component.html',
  styleUrls: ['./players.component.css','/node_modules/semantic-ui-css/semantic.min.css']
})
export class PlayersComponent implements OnInit {



  static jexiaClient = new JexiaService;
  playersDataSet = PlayersComponent.jexiaClient.dataModule.dataset("Players")
  teamDataSet  = PlayersComponent.jexiaClient.dataModule.dataset("Teams");
  players = [];
  teams = [];

  constructor() { }

  

  ngOnInit(): void {

    console.log("hello");

    console.log(PlayersComponent.jexiaClient);
    PlayersComponent.jexiaClient.dataModule.dataset("Players").select().subscribe(records =>{
      console.log("success")
    },
    error => {
      console.log("error");
    });

    this.loadPlayers();
    this.getTeams();
  }

  loadPlayers() {
    this.playersDataSet.select().subscribe(records => {
            this.players = records;
            document.querySelector('#team-segment').classList.remove('loading');
        },
        error => {
            console.log('error');
            document.querySelector('#team-segment').classList.remove('loading');
        });
  }

  getTeams(){
    this.teamDataSet.select().subscribe(records => {
        this.teams = records;
    })

  }

onDeleteUser(e) {
    const id = e.srcElement.closest('div[id]').getAttribute('id');
    document.querySelector('#team-segment').classList.add('loading');
    this.playersDataSet
        .delete()
        .where(field => field('id').isEqualTo(id))
        .subscribe(
            records => {
                this.loadPlayers();
            },
            error => {
                alert('');
                document.querySelector('#team-segment').classList.remove('loading');
            }
        );
}

onInsertUser(e) {
  e.preventDefault();
  const userName = (<HTMLInputElement>document.querySelector('#user-create-form input[name]')).value;
  const userRole = (<HTMLInputElement>document.querySelector('#userRole')).value;
  const userTeam = (<HTMLInputElement>document.querySelector('#teamSelect')).value;
  console.log("yes");
  if (userName.length > 0 && userRole.length > 0 )  {
      console.log("yes");
      document.querySelector('#team-segment').classList.add('loading');
      const insertQuery = this.playersDataSet.insert( {Name : userName, Role:userRole,Team_name: userTeam }).subscribe(records => {
          this.loadPlayers();
          (<HTMLInputElement>document.querySelector('#team-create-form input[name]')).value = '';
          (<HTMLInputElement>document.querySelector('#userRole')).value = '';
      }, error => {
          alert('Erreur lors de la création de la team');
          document.querySelector('#team-segment').classList.remove('loading');
      });
  }
}

}
