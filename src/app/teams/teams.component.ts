import { Component, OnInit } from '@angular/core';
import { JexiaService } from '../jexia.service';

@Component({
  selector: 'app-teams',
  templateUrl: './teams.component.html',
  styleUrls: [
    './teams.component.css',
    '/node_modules/semantic-ui-css/semantic.min.css'
  ]
})
export class TeamsComponent implements OnInit {

  static jexiaClient = new JexiaService();
  teamDataset;

  constructor() { }

  teams = [];

  ngOnInit(): void {
      this.teamDataset = TeamsComponent.jexiaClient.dataModule.dataset('Teams');
      this.loadTeams();
  }
  loadTeams() {
      this.teamDataset.select().subscribe(records => {
              this.teams = records;
              document.querySelector('#team-segment').classList.remove('loading');
          },
          error => {
              console.log('error');
              document.querySelector('#team-segment').classList.remove('loading');
          });
  }
  onDeleteTeamClick(e) {
      const id = e.srcElement.closest('div[id]').getAttribute('id');
      document.querySelector('#team-segment').classList.add('loading');
      this.teamDataset
          .delete()
          .where(field => field('id').isEqualTo(id))
          .subscribe(
              records => {
                  this.loadTeams();
              },
              error => {
                  alert('');
                  document.querySelector('#team-segment').classList.remove('loading');
              }
          );
  }

  onInsertTeamClik(e) {
    e.preventDefault();
    const teamName = (<HTMLInputElement>document.querySelector('#team-create-form input[name]')).value;
    if (teamName.length > 0) {
        document.querySelector('#team-segment').classList.add('loading');
        const insertQuery = this.teamDataset.insert( {name : teamName}).subscribe(records => {
            this.loadTeams();
            (<HTMLInputElement>document.querySelector('#team-create-form input[name]')).value = '';
        }, error => {
            alert('Erreur lors de la création de la team');
            document.querySelector('#team-segment').classList.remove('loading');
        });
    }
  }

}
