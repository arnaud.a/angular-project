import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderTitleComponent } from './header-title/header-title.component';
import { ListSectionComponent } from './list-section/list-section.component';
import { ParticlesComponent } from './particles/particles.component';
import { TemplateFormComponent } from './template-form/template-form.component';
import { PlayersComponent } from './players/players.component';
import { TeamsComponent } from './teams/teams.component';
import { AppRoutingModule } from './app-routing.module';
import { HomepageComponent } from './homepage/homepage.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderTitleComponent,
    ListSectionComponent,
    ParticlesComponent,
    TemplateFormComponent,
    PlayersComponent,
    TeamsComponent,
    HomepageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule 
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
